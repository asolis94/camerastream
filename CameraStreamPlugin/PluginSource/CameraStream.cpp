﻿#include "pch.h"
#include "CameraStream.h"
#include "Debug.h"

//EXTERN_GUID(MFSampleExtension_Spatial_CameraProjectionTransform, 0x47f9fcb5, 0x2a02, 0x4f26, 0xa4, 0x77, 0x79, 0x2f, 0xdf, 0x95, 0x88, 0x6a);
//EXTERN_GUID(MFSampleExtension_Spatial_CameraViewTransform, 0x4E251FA4, 0x830F, 0x4770, 0x85, 0x9A, 0x4B, 0x8D, 0x99, 0xAA, 0x80, 0x9B);

void CameraStream::GetBitmap(IN CONST MediaFrameReference& mediaFrameRef, OUT BYTE* bufferBytes) {
	UINT32 size;
	BYTE* pData{ nullptr };
	SoftwareBitmap bitmap{ nullptr };
	BitmapBuffer bmpBuffer{ nullptr };
	IMemoryBufferReference refBuffer{ nullptr };

	if (mediaFrameRef) {
		bitmap = mediaFrameRef.VideoMediaFrame().SoftwareBitmap();

		if (bitmap) {
			if (!(bitmap.BitmapPixelFormat() == BitmapPixelFormat::Rgba8))
				bitmap = SoftwareBitmap::Convert(bitmap, BitmapPixelFormat::Rgba8);

			bmpBuffer = bitmap.LockBuffer(BitmapBufferAccessMode::Read);
			refBuffer = bmpBuffer.CreateReference();
			refBuffer.as<Windows::Foundation::IMemoryBufferByteAccess>()->GetBuffer(&pData, &size);

			memcpy(bufferBytes, pData, size);
		}
	}
}

void CameraStream::GetProjectionMatrix(IN CONST MediaFrameReference& mediaFrameRef) {
	if (mediaFrameRef.Properties().HasKey(MFSampleExtension_Spatial_CameraProjectionTransform)) {
		Debug::Log(INFO, COUT << "Getting Projection Matrix");

		// Properties:
		//	MFStreamExtension_CameraExtrinsics
		//	MFStreamExtension_PinholeCameraIntrinsics

		auto projectionTransformProperty =
			m_latestFrame.Properties().Lookup(MFSampleExtension_Spatial_CameraProjectionTransform).as<IPropertyValue>();
		winrt::com_array<unsigned char> projectionMatrixByteArray = winrt::com_array<unsigned char>(16);
		projectionTransformProperty.GetUInt8Array(projectionMatrixByteArray);
		float* projectionMatrixValues = reinterpret_cast<float*>(projectionMatrixByteArray.data());

		for (int i = 0; i < 16; ++i) {
			Debug::Log(INFO, COUT << projectionMatrixValues[i]);
		}
	}
	else {
		Debug::Log(INFO, COUT << "No Projection Matrix");
	}

	// ProjectionMatrixValues now contains the 16 entries of the projection matrix
	float4x4 proj = mediaFrameRef.VideoMediaFrame().CameraIntrinsics().UndistortedProjectionTransform();
}

void CameraStream::SetBuffer(IN BYTE* bufferBytes)
{
	m_bufferBytes = bufferBytes;
}

void CameraStream::OnFrameArrived(IN CONST MediaFrameReader& sender, IN CONST MediaFrameArrivedEventArgs& args) {
	if (MediaFrameReference frame = sender.TryAcquireLatestFrame())
	{
		Debug::Log(INFO, "Frame arrived..");
		m_latestFrame = frame;

		GetBitmap(m_latestFrame, m_bufferBytes);
		//GetProjectionMatrix(m_latestFrame);
	}
	else
	{
		Debug::Log(INFO, "Error getting frames");
	}
}

MediaFrameReference CameraStream::GetLatestFrame() {
	return m_latestFrame;
}

void CameraStream::SetSupportedResolutions(IN CONST MediaCapture& mediaCapture)
{
	auto mediaStreamProperties = mediaCapture.VideoDeviceController().GetAvailableMediaStreamProperties(MediaStreamType::VideoPreview);

	for (IMediaEncodingProperties encodingProperties : mediaStreamProperties) {
		m_VideoEncodingProperties.push_back(encodingProperties.as<VideoEncodingProperties>());
	}

	std::sort(m_VideoEncodingProperties.begin(), m_VideoEncodingProperties.end(), []
	(VideoEncodingProperties& videoEncodingProperties1, VideoEncodingProperties& videoEncodingProperties2)
		{
			int length1 = videoEncodingProperties1.Width() * videoEncodingProperties1.Height();
			int length2 = videoEncodingProperties2.Width() * videoEncodingProperties2.Height();

			int framerate1 = videoEncodingProperties1.FrameRate().Numerator() / videoEncodingProperties1.FrameRate().Denominator();
			int framerate2 = videoEncodingProperties2.FrameRate().Numerator() / videoEncodingProperties2.FrameRate().Denominator();

			if (length1 > length2)
			{
				return true;
			}
			else if (length1 == length2 && framerate1 > framerate2)
			{
				return true;
			}
		});

	// Debug
	for (VideoEncodingProperties v : m_VideoEncodingProperties) {
		uint32_t framerate = v.FrameRate().Numerator() / v.FrameRate().Denominator();

		Debug::Log(INFO, COUT
			<< "Wdith: " << v.Width()
			<< ", Height: " << v.Height()
			<< ", FrameRate: " << framerate
			<< ", Encoding Type: " << winrt::to_string(v.Subtype()));
	}
}

task<std::shared_ptr<CameraStream>> CameraStream::CreateAsync() {
	// TODO:	Find and set desired resolution
	// TODO: 	Obtain frames with set resolution
	// TODO: 	Obtain projection matrix
	// TODO: 	Obtain camtoworld matrix
	// TODO: 	Callback to process frames

	return create_task([]() {
		auto groups = MediaFrameSourceGroup::FindAllAsync().get();

		MediaFrameSourceGroup selectedGroup = nullptr;
		MediaFrameSourceInfo selectedSourceInfo = nullptr;

		for (MediaFrameSourceGroup sourceGroup : groups) {
			for (MediaFrameSourceInfo sourceInfo : sourceGroup.SourceInfos()) {
				if (sourceInfo.SourceKind() == MediaFrameSourceKind::Color) {
					selectedSourceInfo = sourceInfo;
					break;
				}
			}
			if (selectedSourceInfo != nullptr)
			{
				selectedGroup = sourceGroup;
				break;
			}
		}

		// No valid camera was found. This will happen on the emulator.
		if (selectedGroup == nullptr || selectedSourceInfo == nullptr)
		{
			Debug::Log(INFO, COUT << "No valid camera was found");
			return task_from_result(std::shared_ptr<CameraStream>(nullptr));
		}

		auto settings = MediaCaptureInitializationSettings();
		settings.MemoryPreference(MediaCaptureMemoryPreference::Cpu);
		settings.StreamingCaptureMode(StreamingCaptureMode::Video);
		settings.SourceGroup(selectedGroup);

		auto mediaCapture = MediaCapture();
		mediaCapture.InitializeAsync(settings).get();

		auto selectedSource = mediaCapture.FrameSources().Lookup(selectedSourceInfo.Id());

		auto supportedFormats = selectedSource.SupportedFormats();

		for (MediaFrameFormat supportedFormat : supportedFormats) {
			uint32_t framerate = supportedFormat.FrameRate().Numerator() / supportedFormat.FrameRate().Denominator();

			if (supportedFormat.VideoFormat().Width() != 1280 || supportedFormat.VideoFormat().Height() != 720 || framerate != 30)
				continue;

			Debug::Log(INFO, COUT
				<< "Wdith: " << supportedFormat.VideoFormat().Width()
				<< ", Height: " << supportedFormat.VideoFormat().Height()
				<< ", FrameRate: " << framerate
				<< ", Encoding Type: " << winrt::to_string(supportedFormat.Subtype()));

			selectedSource.SetFormatAsync(supportedFormat).get();

			break;
		}

		// TODO:	Set custom resolution and color map
		//auto mediaFrameReader = mediaCapture.CreateFrameReaderAsync(
		//	selectedSource,
		//	MediaEncodingSubtypes::Mjpg(),
		//	BitmapSize{ 1280,720 }
		//).get();

		Debug::Log(INFO, COUT << winrt::to_string(selectedSource.CurrentFormat().Subtype()));

		auto mediaFrameReader = mediaCapture.CreateFrameReaderAsync(selectedSource).get();

		if (mediaFrameReader)
		{
			mediaFrameReader.AcquisitionMode(MediaFrameReaderAcquisitionMode::Realtime);
			return task_from_result(std::make_shared<CameraStream>(mediaCapture, mediaFrameReader, selectedSource));
		}
		else
		{
			return task_from_result(std::shared_ptr<CameraStream>(nullptr));
		}
		});
}

void CameraStream::StartVideoReaderAsync() {
	auto status = m_mediaFrameReader.StartAsync().get();

	if (status == MediaFrameReaderStartStatus::Success)
	{
		Debug::Log(INFO, COUT << "Media capture start created");
		m_mediaFrameReader.FrameArrived({ this, &CameraStream::OnFrameArrived });
	}
	else
	{
		Debug::Log(INFO, COUT << "Media capture start failed");
	}
}

void CameraStream::StopVideoReaderAsync() {
	m_mediaFrameReader.StopAsync().get();
}

CameraStream::CameraStream(IN MediaCapture mediaCapture, IN MediaFrameReader reader, IN MediaFrameSource source) :
	m_mediaCapture(mediaCapture),
	m_mediaFrameReader(reader),
	m_mediaFrameSource(source)
{
	std::string info = winrt::to_string((m_mediaFrameSource.Info().DeviceInformation().Name()));
	Debug::Log(INFO, COUT << info);

	//SetSupportedResolutions(m_mediaCapture);
}