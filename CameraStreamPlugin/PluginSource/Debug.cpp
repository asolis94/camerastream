
#include "pch.h"
#include "Debug.h"

template<typename T>
void Debug::Log(std::stringstream info, T t) {
	debugStream << info.str() << ": " << std::endl
		<< t
		<< std::endl << std::endl;
}
template void Debug::Log<std::string>(std::stringstream info, std::string s);
template void Debug::Log<const char*>(std::stringstream info, const char* c);
template void Debug::Log<float>(std::stringstream info, float m);

std::ostringstream Debug::debugStream;