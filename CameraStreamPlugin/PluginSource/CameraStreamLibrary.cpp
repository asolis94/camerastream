#include "pch.h"
#include "CameraStreamLibrary.h"
#include "Debug.h"
#include "CameraStream.h"

std::shared_ptr<CameraStream> cameraStreamPtr = nullptr;

VOID InitCameraStream()
{
	if (cameraStreamPtr)  return;
	cameraStreamPtr = CameraStream::CreateAsync().get();
}

VOID SetStreamingBuffer(BYTE* bufferBytes)
{
	if (!cameraStreamPtr)  return;
	cameraStreamPtr->SetBuffer(bufferBytes);
}

VOID StartWebcam()
{
	if (!cameraStreamPtr)  return;
	cameraStreamPtr->StartVideoReaderAsync();
}

VOID StopWebcam()
{
	if (!cameraStreamPtr)  return;
	cameraStreamPtr->StopVideoReaderAsync();
}

BSTR GetDebugLog()
{
	std::string narrowstr(Debug::debugStream.str());
	std::wstring widestr = std::wstring(narrowstr.begin(), narrowstr.end());

	Debug::Clear();

	return ::SysAllocString(widestr.c_str());
}