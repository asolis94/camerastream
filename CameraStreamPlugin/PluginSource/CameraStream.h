﻿#pragma once
#ifndef CAMSTREAM_H
#define CAMSTREAM_H

using namespace concurrency;
using namespace winrt::Windows::Foundation;
using namespace winrt::Windows::Foundation::Collections;
using namespace winrt::Windows::Foundation::Numerics;
using namespace winrt::Windows::Perception;
using namespace winrt::Windows::Perception::Spatial;
using namespace winrt::Windows::Perception::Spatial::Preview;
using namespace winrt::Windows::Media;
using namespace winrt::Windows::Media::Capture;
using namespace winrt::Windows::Media::Capture::Frames;
using namespace winrt::Windows::Media::Devices::Core;
using namespace winrt::Windows::Media::MediaProperties;
using namespace winrt::Windows::Graphics::Imaging;


class CameraStream {
public:

	//vector<Resolution> supportedResolutions;

	static task<std::shared_ptr<CameraStream>> CreateAsync();

	MediaFrameReference GetLatestFrame();

	void GetBitmap(
		IN CONST MediaFrameReference& mediaFrameRef,
		OUT BYTE* bufferBytes
	);

	void GetProjectionMatrix(
		IN CONST MediaFrameReference& mediaFrameRef
	);

	void SetBuffer(
		IN BYTE* bufferBytes
	);

	void StartVideoReaderAsync();
	void StopVideoReaderAsync();


	CameraStream(
		IN MediaCapture mediaCapture,
		IN MediaFrameReader reader,
		IN MediaFrameSource source
	);

private:
	BYTE* m_bufferBytes;
	MediaCapture m_mediaCapture;
	MediaFrameSource m_mediaFrameSource;
	MediaFrameReader m_mediaFrameReader;
	MediaFrameReference m_latestFrame{ nullptr };
	std::vector<VideoEncodingProperties> m_VideoEncodingProperties;

	void OnFrameArrived(
		IN CONST MediaFrameReader& sender,
		IN CONST MediaFrameArrivedEventArgs& args
	);

	void SetSupportedResolutions(
		IN CONST MediaCapture& mediaCapture
	);
};

#endif