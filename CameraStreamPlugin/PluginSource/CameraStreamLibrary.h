#pragma once

#define APIEXPORT __declspec(dllexport)
#define EXTERN extern "C"


EXTERN APIEXPORT VOID InitCameraStream();
EXTERN APIEXPORT VOID SetStreamingBuffer(BYTE* texturedataPtr);
EXTERN APIEXPORT VOID StartWebcam();
EXTERN APIEXPORT VOID StopWebcam();
EXTERN APIEXPORT BSTR GetDebugLog();