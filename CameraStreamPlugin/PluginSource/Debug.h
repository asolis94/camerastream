#pragma once
#ifndef DEBUG_H
#define DEBUG_H


#define COUT std::stringstream()
#define INFO  std::stringstream() << __FUNCTION__ << "|" << __LINE__

class Debug {
public:
	static std::ostringstream debugStream;

	template<typename T>
	static void Log(std::stringstream info, T t);

	static void Log(std::stringstream info, std::stringstream ss) {
		debugStream << info.str() << ": " << std::endl
			<< ss.str()
			<< std::endl << std::endl;
	}

	static void Clear() {
		debugStream.str("");
		debugStream.clear();
	}

};


#endif