using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class PluginImport : MonoBehaviour
{
    [DllImport("ASimplePlugin")]
    public static extern void InitCameraStream();

    [DllImport("ASimplePlugin")]
    public static extern void StartWebcam();
    [DllImport("ASimplePlugin")]
    public static extern void StopWebcam();

    [DllImport("ASimplePlugin")]
    public static extern void SetStreamingBuffer(byte[] bytes);

    [DllImport("ASimplePlugin")]
    [return: MarshalAs(UnmanagedType.BStr)]
    public static extern string GetDebugLog();


    public GameObject quadGameObject;
    Resolution webcamResolution;
    Texture2D textureWebcam;
    byte[] rawBytes;


    void Start()
    {
        webcamResolution = new Resolution() { width = 1280, height = 720 };
        rawBytes = new byte[webcamResolution.width * webcamResolution.height * 4];
        textureWebcam = new Texture2D(webcamResolution.width, webcamResolution.height, TextureFormat.RGBA32, false);
        quadGameObject.GetComponent<Renderer>().material.mainTexture = textureWebcam;

        InitCameraStream();
        SetStreamingBuffer(rawBytes);
        StartWebcam();
    }

    void Update()
    {
        textureWebcam.LoadRawTextureData(rawBytes);
        textureWebcam.Apply();

        string log = GetDebugLog();

        if (log.Length > 0)
            Debug.Log(log);
    }

    void OnApplicationQuit()
    {
        StopWebcam();
    }
}
